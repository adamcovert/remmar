$(document).ready(function () {

  var rellax = new Rellax('.rellax');



  $('.page-header__burger').on('click', function () {
    $('.page-header__mobile-menu').addClass('page-header__mobile-menu--is-open');
  });

  $('.page-header__mobile-menu-close').on('click', function () {
    $('.page-header__mobile-menu').removeClass('page-header__mobile-menu--is-open');
  });



  var swiper = new Swiper('.promo__slider', {
    effect: 'fade',
    fadeEffect: {
      crossFade: true
    },
    pagination: {
      el: '.promo__slider-pagination',
      clickable: true
    },
    navigation: {
      prevEl: '.promo__slider-navigation .slider-navigation__btn--prev',
      nextEl: '.promo__slider-navigation .slider-navigation__btn--next'
    },
  });



  var swiper = new Swiper ('.advantages__slider', {
    spaceBetween: 20,
    pagination: {
      el: '.advantages__slider-pagination',
      clickable: true
    },
    breakpoints: {
      320: {
        slidesPerView: 1
      },
      480: {
        slidesPerView: 2
      },
      992: {
        slidesPerView: 4,
        allowTouchMove: false
      }
    }
  });



  var swiper = new Swiper ('.services__slider', {
    spaceBetween: 20,
    pagination: {
      el: '.services__slider-progressbar',
      type: 'progressbar'
    },
    navigation: {
      prevEl: '.services__slider-navigation .slider-navigation__btn--prev',
      nextEl: '.services__slider-navigation .slider-navigation__btn--next'
    },
    breakpoints: {
      320: {
        slidesPerView: 1
      },
      600: {
        slidesPerView: 2
      },
      992: {
        slidesPerView: 3
      }
    }
  });



  var swiper = new Swiper ('.products__slider', {
    spaceBetween: 20,
    pagination: {
      el: '.products__slider-progressbar',
      type: 'progressbar'
    },
    navigation: {
      prevEl: '.products__slider-navigation .slider-navigation__btn--prev',
      nextEl: '.products__slider-navigation .slider-navigation__btn--next'
    },
    breakpoints: {
      320: {
        slidesPerView: 1
      },
      414: {
        slidesPerView: 2
      },
      768: {
        slidesPerView: 3
      },
      992: {
        slidesPerView: 4
      }
    }
  });



  var swiper = new Swiper ('.clients__slider', {
    spaceBetween: 20,
    pagination: {
      el: '.clients__slider-pagination',
      clickable: true
    },
    breakpoints: {
      320: {
        slidesPerView: 1
      },
      480: {
        slidesPerView: 2
      },
      992: {
        slidesPerView: 4,
        allowTouchMove: false
      }
    }
  });



  var breakpoint = window.matchMedia( '(min-width: 992px)' );
  var mySwiper;
  var docSwiperOptions = {
    spaceBetween: 20,
    pagination: {
      el: '.docs__block .docs__slider-progressbar',
      type: 'progressbar'
    },
    breakpoints: {
      320: {
        slidesPerView: 1
      },
      480: {
        slidesPerView: 2
      },
      768: {
        slidesPerView: 3
      },
      992: {
        slidesPerView: 4
      }
    }
  };

  var breakpointChecker = function () {
    if ( breakpoint.matches === true ) {
  	  if ( mySwiper !== undefined ) mySwiper.destroy( true, true );
	    return;
    } else if ( breakpoint.matches === false ) {
      return enableSwiper();
    }
  };

  var enableSwiper = function () {
    mySwiper = new Swiper('.docs__block .docs__slider', docSwiperOptions);
  };

  breakpoint.addListener(breakpointChecker);
  breakpointChecker();



  var swiper = new Swiper ('.service__slider', {
    effect: 'fade',
    fadeEffect: {
      crossFade: true
    },
    pagination: {
      el: '.service__slider-pagination',
      clickable: true
    },
    navigation: {
      prevEl: '.service__slider-navigation .slider-navigation__btn--prev',
      nextEl: '.service__slider-navigation .slider-navigation__btn--next'
    }
  });
});